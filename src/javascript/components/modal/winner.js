import { showModal } from './modal';
export function showWinnerModal(fighter) {
  // call showModal function
  const winnerInfo = {
    title: 'WINNER OF THE GAME',
    bodyElement: fighter.name
  };

  showModal(winnerInfo);
  //
}
