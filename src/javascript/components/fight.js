import { controls } from '../../constants/controls';
import { createElement } from '../helpers/domHelper';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over

    const healthContainer = document.getElementsByClassName('arena___health-bar');
    const healthBars = [...healthContainer];
    const statusContainer = document.getElementsByClassName('arena___health-indicator');
    const status = [...statusContainer];
    const statusInfo = {
      block: false,
      currentHealth: 100,
      timeOfCrit: Date.now(),
      critInput: []
    };

    const playerOne = {
      ...firstFighter,
      ...statusInfo,
      healthBar: healthBars[0],
      status: status[0],
      position: 'left'
    };

    const playerTwo = {
      ...secondFighter,
      ...statusInfo,
      healthBar: healthBars[1],
      status: status[1],
      position: 'right'
    };

    function showStatus(fighter, text) {
      if (document.getElementById(`${fighter.position}-status-marker`)) {
        document.getElementById(`${fighter.position}-status-marker`).remove();
      }

      const statusHandler = createElement({
        tagName: 'div',
        className: 'arena___status-marker',
        attributes: { id: `${fighter.position}-status-marker` }
      });
      statusHandler.innerText = text;
      statusHandler.style.opacity = '1';
      fighter.status.append(statusHandler);
      setInterval(() => {
        if (statusHandler.style.opacity > 0) {
          statusHandler.style.opacity = statusHandler.style.opacity - 0.01;
        } else {
          statusHandler.remove();
        }
      }, 20);
    }

    function attackRelease(attacker, defender) {
      const totalDamage = getDamage(attacker, defender);

      showStatus(defender, `-${totalDamage.toFixed(1)}`);
      defender.currentHealth = defender.currentHealth - (totalDamage / defender.health) * 100;
      if (defender.currentHealth <= 0) {
        document.removeEventListener('keydown', onDown);
        document.removeEventListener('keyup', onUp);
        resolve(attacker);
      }

      defender.healthBar.style.width = `${defender.currentHealth}%`;
    }

    function onDown(event) {
      if (!event.repeat) {
        switch (event.code) {
          case controls.PlayerOneAttack: {
            attackRelease(playerOne, playerTwo);
            break;
          }

          case controls.PlayerTwoAttack: {
            attackRelease(playerTwo, playerOne);
            break;
          }

          case controls.PlayerOneBlock: {
            playerOne.block = true;
            break;
          }

          case controls.PlayerTwoBlock: {
            playerTwo.block = true;
            break;
          }
        }
      }
    }

    function onUp(event) {
      switch (event.code) {
        case controls.PlayerOneBlock:
          playerOne.block = false;
          break;
        case controls.PlayerTwoBlock:
          playerTwo.block = false;
          break;
      }

      if (playerOne.critInput.includes(event.code)) {
        playerOne.critInput.splice(playerOne.critInput.indexOf(event.code), 1);
      }

      if (playerTwo.critInput.includes(event.code)) {
        playerTwo.critInput.splice(playerTwo.critInput.indexOf(event.code), 1);
      }
    }

    document.addEventListener('keydown', onDown);
    document.addEventListener('keyup', onUp);
  });
}

export function getDamage(attacker, defender) {
  // return damage
  const damage = getHitPower(attacker) - getBlockPower(defender);
  return damage > 0 ? damage : 0;
}

export function getHitPower(fighter) {
  // return hit power
  const criticalHitChance = fighter.critInput === 3 ? 2 : Math.random() + 1;
  return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  // return block power
  const dodgeChance = Math.random() + 1;
  return fighter.defense * dodgeChance;
}
